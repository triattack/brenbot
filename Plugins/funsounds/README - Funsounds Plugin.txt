Funsounds Plugin
Version 1.01
Author: Anonymous


Plays sounds ingame when certain triggers are seen in F2 / F3 chat. Based on mIRC
scripts which do the same thing.

If you want to ask any questions, request new features or report bugs please post on the Renegade Public forums (www.renegadeforums.com) in the Technical Support -> Other area, or email me.


###############
Installation
###############

Extract the plugins folder to your BRenBot folder.


###############
Changelog
###############

1.02
 - Added config options for setting which text events it triggers on (f2, f3 or irc)
 - Removed unneccesary debug messages

1.01
 - Support for updating through brLoader

1.0
 - First Release


###############
Commands
###############

N/A