<?php
/* This file contains the connection settings for your mysql database, where
the rank data will be stored. If you do not wish to use the searching and sorting
features of the ranks page, or do not have a mysql database, set the $dbhost to -1 */

$dbhost = 'localhost';
$dbname = 'database';
$dbuser = 'username';
$dbpasswd = 'password';

?>